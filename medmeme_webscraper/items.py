# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy.item import Field


class MedmemeWebscraperItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()

    url = Field()
    statuscode = Field()
    project = Field()
    spider = Field()
    server = Field()
    date = Field()
    schema = Field(key='schema', value='http://json-schema.org/draft-06/schema#')
    title = Field(key='title', value='URL Metadata')
    description = Field(key='description', value='Contains metadata scraped url')
    domain_name = Field()
    data_download_location = Field()
    filename = Field()
