# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
from scraper_utilities import formatItemtodb
from scraper_utilities import db_connect
from scraper_utilities import get_logger
import psycopg2
import datetime

logger = get_logger(__name__)


class MedmemeWebscraperPipeline(object):
    def process_item(self, item, spider):
        return item


class PostgresqlPipeline(object):
    """Pipeline that writes scrapy items to postgresql"""
    conn = db_connect()
    cur = conn.cursor()
    started_on = ''

    def process_item(self, item, spider):
        item_tostring = str(item)
        try:
            self.cur.execute(
                    "UPDATE spider_trace set spider_name='%s', server_name='%s' where spider_opened='%s'" % (item['spider'][0], item['server'][0], self.started_on.isoformat()))
            self.cur.execute("UPDATE url_attributes set processmeta=%s, download_location='%s', filename='%s' where url='%s'" %
                             (formatItemtodb(item_tostring), item['data_download_location'][0], item['filename'][0], item['url'][0]))
        except psycopg2.Error as e:
            logger.info(e)
        finally:
            self.conn.commit()

    def open_spider(self, spider):
        self.started_on = datetime.datetime.now()
        try:
            self.cur.execute("INSERT into spider_trace(spider_opened) values ('%s')" % self.started_on.isoformat())
            self.cur.execute("UPDATE seed_urls set status='WIP' where status='R'")
        except psycopg2.Error as e:
            logger.info(e)
        finally:
            self.conn.commit()

    def close_spider(self, spider):
        closed_on = datetime.datetime.now()
        work_time = datetime.datetime.now() - self.started_on
        try:
            self.cur.execute("UPDATE spider_trace set spider_closed='%s' where spider_opened='%s'" % (closed_on.isoformat(), self.started_on.isoformat()))
            self.cur.execute("UPDATE spider_trace set total_runtime='%s' where spider_opened='%s'" % (work_time, self.started_on.isoformat()))
            self.cur.execute("UPDATE seed_urls set status='C' where status='WIP'")
        except psycopg2.Error as e:
            logger.info(e)
        finally:
            self.conn.commit()
            self.cur.close()
            self.conn.close()
