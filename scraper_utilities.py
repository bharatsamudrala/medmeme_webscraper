import psycopg2
from psycopg2.extensions import adapt
import os
import glob
import time
import logging
from boto3 import Session
import shutil
import datetime

get_starturl_query = "select distinct url from seed_urls where active=TRUE and status='R' and dynamic=FALSE and manual=FALSE ;"
get_filters_query = "select distinct filter from seed_urls where active=TRUE and status='R' and dynamic=FALSE and manual=FALSE ;"
get_allowed_domain_query = "select distinct domain_name from seed_urls where active=TRUE and status='R';"


def get_logger(filename):
    logging.basicConfig(filename=filename, level=logging.DEBUG,
                        format='%(asctime)s \t[%(name)-s] \t%(filename)-s \t%(lineno)-d'
                               ' \t%(levelname)-s: \t%(message)s \r',
                        datefmt='%Y-%m-%dT%T%Z')
    logger = logging.getLogger(filename)

    return logger


log_name = time.strftime("%Y-%m-%dT%T%Z")
s3_log_key = os.path.join('logs', datetime.datetime.now().strftime('%Y/%m/%d'), 'unstructured', 'raw', log_name)
bucket = 'nexscope-safety'
logger = get_logger(log_name)


def uploadLogfile():
    s3_client = get_s3_client()
    file_location = os.path.join(os.getcwd(), log_name)
    if not s3_exists(s3_client, bucket, s3_log_key):
        s3_client.upload_file(file_location, bucket, s3_log_key)
        os.remove(file_location)
        return


def db_connect():
    try:
        conn = psycopg2.connect(host='18.217.130.132', user='postgres', password='pos123', dbname='medmeme')
        return conn
    except psycopg2.Error as e:
        logger.error("Unable establish database connection")
        logger.error(e)
    except psycopg2.Warning as w:
        logger.warning(w)

def getStartUrls():
    fetchUrls = []
    try:
        conn = db_connect()
        cur = conn.cursor()
        cur.execute(get_starturl_query)
        urls = cur.fetchall()
        for url in urls:
            fetchUrls.append(url[0])
        cur.close()
        conn.close()
        return fetchUrls
    except psycopg2.OperationalError as e:
        logger.info("Could not connect to server. Check the database connections")
        logger.info(e)


def getFilters():
    filter_list = []
    try:
        conn = db_connect()
        cur = conn.cursor()
        cur.execute(get_filters_query)
        urls = cur.fetchall()
        for url in urls:
            filter_list.append(url[0])
        cur.close()
        conn.close()
        return filter_list
    except psycopg2.OperationalError as e:
        logger.info("Could not connect to server. Check the database connections")
        logger.info(e)


def getAllowedDoamins():
    fetchDomains = []
    try:
        conn = db_connect()
        cur = conn.cursor()
        cur.execute(get_allowed_domain_query)
        urls = cur.fetchall()
        for url in urls:
            fetchDomains.append(url[0])
        cur.close()
        conn.close()
        return fetchDomains
    except psycopg2.OperationalError as e:
        logger.info("Could not connect to server. Check the database connections")
        logger.info(e)


def createfile(filename):
    download_dir = os.getcwd()
    filex = open(os.path.join(download_dir, filename), 'wb')
    return filex

def formatItemtodb(item):
    data = adapt(item)
    return data

def get_s3_client():
    session = Session(
        aws_access_key_id=os.getenv('AWS_ACCESS_KEY_ID'),
        aws_secret_access_key=os.getenv('AWS_SECRET_ACCESS_KEY')
    )

    return session.client('s3')


def s3_exists(client, bucket, key):
    resp = client.list_objects(
        Bucket=bucket,
        Prefix=key)
    expected_object = resp.get('Contents', [])

    return bool(expected_object)


def uploadToS3(file_location, bucket, s3_file_key):
    s3_client = get_s3_client()
    if not s3_exists(s3_client, bucket, s3_file_key):
        s3_client.upload_file(file_location, bucket, s3_file_key)
        logger.info('Saved {} to download directory'.format(file_location))
        return True
    else:
        logger.info('File {} already exists in S3' .format(file_location))
        return False